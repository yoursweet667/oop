import csv


def sortlist():
    with open('movies.csv', encoding='UTF-8') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=";")
        try:
         ##   print(csvfile.read())
            sortedlist = sorted(spamreader, key=lambda row: (row['movie_id']), reverse=False)
            return sortedlist
        except UnicodeEncodeError:
            pass

with open('sorted.csv', 'w') as f:
    fieldnames = ['movie_id', 'name_rus', 'kp_rating', 'movie_duration', 'kp_rating_count', 'movie_year', 'imdb_rating',
                  'imdb_rating_count', 'genres', 'countries', 'budget', 'critics_rating', 'name_eng']
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    sortedlist = sortlist()
    for row in sortedlist:
        writer.writerow(row)