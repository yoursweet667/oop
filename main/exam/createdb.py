import sqlite3

db = sqlite3.connect("movies.db")
sql = db.cursor()

sql.execute("""CREATE TABLE IF NOT EXISTS movies (
    movie_id INT,
    name_rus TEXT,
    kp_rating  FLOAT,
    movie_duration INT,
    kp_rating_count FLOAT,
    movie_year TEXT
    imdb_rating FLOAT,
    imdb_rating_count FLOAT,
    genres TEXT,
    countries TEXT,
    budget TEXT,
    critics_rating FLOAT,
    name_eng TEXT
    )""")

sql.execute("SELECT login FROM users")
if sql.fetchone() is None:
    sql.execute(f"INSERT INTO users VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", (   ))
    db.commit()

    for value in sql.execute("SELECT * FROM users"):
        print(value)