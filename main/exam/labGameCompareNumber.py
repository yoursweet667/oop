# -*- coding: cp1251 -*-

import time
from random import randint


def choseDifficult():
    difficult = str(input('�������� ������� ���������: 1) ������, 2) �������, 3) �������: '))
    return difficult

def choseMode():
    mode = str(input('�������� ����� ����: ��������� ����������� ����� � 1) �������� ������ ��� 2) ����������������� ������: '))
    times = int(input('���������� ��������?'))
    if mode == "��������" :
        binary(choseDifficult(), times)
    elif mode =="�����������������" :
        hexadecimal(choseDifficult(), times)
    else: print("������ ������ ���� ���!")



correct = 0
incorrect = 0

def counter(answer, first, second):
    global correct
    global incorrect

    if answer == "=":
        if first == second:
            correct += 1
        else:
            incorrect += 1

    if answer == ">":
        if first > second:
            correct += 1
        else:
            incorrect += 1

    if answer == "<":
        if first < second:
            correct += 1
        else:
            incorrect += 1



def binary(difficult, times):

    match difficult:

        case "������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                second = bin(randint(0, 15))
                first = randint(0, 15)
                print(f'{first} ? {second}')
                answer = input("������� �����: ")
                counter(answer, bin(first), second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case "�������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                second = bin(randint(0, 63))
                first = randint(0, 63)
                print(f'{first} ? {second}')
                answer = input("������� �����: ")
                counter(answer, bin(first), second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case "�������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                second = bin(randint(0, 255))
                first = randint(0, 255)
                print(f'{first} ? {second}')
                answer = input("������� �����: ")
                counter(answer, bin(first), second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case default:
            print("����� ��������� ���!")



def hexadecimal(difficult, times):

    match difficult:

        case "������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                first = randint(0, 15)
                second = randint(0, 15)
                print(f'{first} ? {hex(second)}')
                answer = input("������� �����: ")
                counter(answer, first, second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case "�������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                first = randint(0, 63)
                second = randint(0, 15)
                print(f'{first} ? {hex(second)}')
                answer = input("������� �����: ")
                counter(answer, first, second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case "�������":
            start_time = time.monotonic()
            for i in range(0, int(times)):
                first = randint(0, 255)
                second = randint(0, 15)
                print(f'{first} ? {hex(second)}')
                answer = input("������� �����: ")
                counter(answer, first, second)
            print(f"����� ����: {int(time.monotonic() - start_time)} ������.")
            print(f"���������� �������: {correct}")
            print(f"������������ �������: {incorrect}")

        case default:
            print("����� ��������� ���!")



if __name__ == "__main__":
    choseMode()