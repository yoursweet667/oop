# -*- coding: cp1251 -*-
import csv
import os
import sqlite3
import time

from Model.FileData import FileData

path = 'C:\Program Files (x86)\Steam'
db = sqlite3.connect("storage.db")
sql = db.cursor()
result = []




def scanFolder(path):
    entries = os.listdir(path)

    if not entries:
        return None

    else:
        for entry in entries:
            entry_path = path + '/' + entry

            if os.path.isfile(entry_path):
                getFileData(entry, path, entry_path)

            else:
                writeFolderDB(entry_path)
                scanFolder(entry_path)

        return result


def getFileData(file, file_path, entry_path):
    destination = file_path + '/' + file
    btyeSize = os.stat(destination).st_size
    modificationDatetime = os.path.getmtime(destination)
    creationDateTime = os.path.getctime(destination)
    extension = file.split(".")

    if len(extension) > 1:
        addDataToList(extension[0], extension[1], btyeSize, entry_path, creationDateTime, modificationDatetime)


def addDataToList(name, extension, size, location, creationDateTime, modificationDatetime):
    fileData = FileData(name, extension, size, location, time.ctime(creationDateTime),
                        time.ctime(modificationDatetime))
    result.append(fileData)
    writeDataDB(fileData)


def writeToCSV():
    sql.execute(f"SELECT * FROM files_data")
    rows = sql.fetchall()

    with open('data.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(
            ['key', 'name', 'extension', 'size', 'file_path', 'folder', 'creation_time', 'modification_time'])
        for row in rows:
            writer.writerow(row)


def writeDataDB(fileData):
    name = fileData.name
    extension = fileData.extension
    size = fileData.byte_size
    location = fileData.location
    file_path = f"{location}/{name}.{extension}"
    creation_time = fileData.creationDateTime
    modification_time = fileData.modificationDatetime

    createFilesDB()

    sql.execute(
        f"INSERT INTO files_data VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        (None, name, extension, size, file_path, location, creation_time, modification_time))
    db.commit()

    print(file_path)


def writeFolderDB(entry_path):
    createFoldersDB()

    sql.execute(
        f"INSERT OR IGNORE INTO folders VALUES (?,?,?)",
        (None, entry_path, os.path.getsize(entry_path))
    )


def createFoldersDB():
    sql.executescript("""create table IF NOT EXISTS folders
(
    key         integer not null
        constraint folders_pk
            primary key autoincrement,
    folder_path TEXT    not null,
    folder_size integer not null
);

create unique index IF NOT EXISTS folders_folder_path_uindex
    on folders (folder_path);

create unique index IF NOT EXISTS folders_key_uindex
    on folders (key);""")


def createFilesDB():
    sql.executescript("""create table IF NOT EXISTS files_data
(
    key               integer  not null
        constraint files_data_pk
            primary key autoincrement,
    name              TEXT     not null,
    extension         TEXT     not null,
    size              DECIMAL  not null,
    file_path         TEXT     not null,
    folder            TEXT     not null,
    creation_time     DATETIME not null,
    modification_time DATETIME not null
);

create unique index IF NOT EXISTS files_data_file_path_uindex
    on files_data (file_path);

create unique index IF NOT EXISTS files_data_key_uindex
    on files_data (key);
""")
    db.commit()


def cleanDB():
    sql.execute("""DELETE FROM files_data""")
    sql.execute("""DELETE FROM sqlite_sequence WHERE name='files_data';""")
    sql.execute("""DELETE FROM folders""")
    sql.execute("""DELETE FROM sqlite_sequence WHERE name='folders';""")

if __name__ == "__main__":
    createFilesDB()
    createFoldersDB()
    cleanDB()
    path = input("������� ���� ���������: ")
    print("������ ������������...")
    scanFolder(path)
    print("������������ ���������!")
    writeToCSV()
    print("������ ���� data.csv")
