## todo: Нарисовать в консоли прямоугольную улитку с параметрами: ширина, высота

import turtle
from turtle import *
turtle.pensize(3)

a = int(input("Введите ширину: "))
b = int(input("Введите высоту: "))

while (a > 0 and b > 0):
    forward(a)
    right(90)
    a -= 5
    forward(b)
    right(90)
    b -= 5
a -= 5
forward(a)
done()