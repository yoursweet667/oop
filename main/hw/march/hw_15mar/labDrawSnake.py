## todo: Нарисовать в консоли прямоугольную змейку с параметрами: ширина, высота

import turtle
from turtle import *
turtle.pensize(3)

a = int(input("Введите ширину: "))
b = int(input("Введите высоту: "))

while (b > 0):
    forward(a)
    right(90)
    forward(5)
    b -= 5
    right(90)
    forward(a)
    left(90)
    forward(5)
    b -= 5
    left(90)
forward(a)
done()