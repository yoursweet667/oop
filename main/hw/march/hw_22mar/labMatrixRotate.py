# todo: Создать матрицу, заполнить случайными значениями, например от 10 до 99,
# todo: распечатать его и затем в цикле выбирать действия над матрицей,
# todo: например: повернуть по часов, повернуть против часов, отразить по вертикали и тп. и печатать результат.
import random

def printArray(array):
    for i in range(len(array)):
        for j in range(len(array[i])):
            print(array[i][j], end=' ')
        print()
    print()

## по часовой стрелке
def rotateArray(array):
    rotatedArray = tuple(zip(*array[::-1]))
    printArray(rotatedArray)

## по вертикали
def mirrorArray(array):
    mirroredArray = list(array[::-1])
    printArray(mirroredArray)

r = 0
x = int(input('Введите количество строк: '))
y = int(input('Введите количество столбцов: '))
array = []
for i in range(x):
    array.append([])
    for j in range(y):
        array[i].append(random.randint(10, 99))
        r += 1
printArray(array)

action = str(input('Матрицу можно отразить или повернуть, что будем делать? '))

match action:
    case "повернуть":
        rotateArray(array)
    case "отразить":
        mirrorArray(array)
    case default:
        print('Мы пока что не научились так делать! Возвращайствь позже :)')