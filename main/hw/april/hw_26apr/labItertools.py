## todo: создать примеры на другие итераторы, которые мы не рассмотрели на занятиях
import itertools
import operator

## Бесконечный счётчик
cnt = itertools.count(start=0, step=5)
for i in range (0, 11):
    print(next(cnt));
print()

## Аккумулирующий итератор
print(list(itertools.accumulate(range(1, 10), operator.mul)))
print()

## Бесконечный итератор последовательности
waltz = itertools.cycle(['Java', 'не', 'тормозит!'])
for i in range (0, 12):
    print(next(waltz));
print()

## Бесконечный итератор одного объекта
s = "Write Once, Run Anywhere!"
rep = itertools.repeat(s, times=5)
for i in range (0, 5):
    print(next(rep));
print()

## Плоский список из вложенного, удобно использовать для объединения списков
print(list(itertools.chain('ABC', 'DEF')))
print()

## Комбинаторика: размещение с повторениями
digits = range(2)
pincode_vars = itertools.product(digits, repeat=3)
for var in pincode_vars:
    print(var)
print()

## Фильтрация группы элементов
numbers = [0, 1, 2, 3, 2, 1, 0]
selectors = [True, True, False, True]
print(list(itertools.compress(numbers, selectors)))
print()

##Группировка по ключу
people = [{"Имя": "Петр",
           "Отчество": "Петрович",
           "Фамилия": "Петров",
           "Оценка":5},
          {"Имя": "Ольга",
           "Отчество": "Алексеевна",
           "Фамилия": "Иванова",
           "Оценка":5},
          {"Имя": "Николай",
           "Отчество": "Николаевич",
           "Фамилия": "Николаев",
           "Оценка":4},
          {"Имя": "Федор",
           "Отчество": "Владимирович",
           "Фамилия": "Иванов",
           "Оценка":3},
          {"Имя": "Владимир",
           "Отчество": "Федорович",
           "Фамилия": "Иванов",
           "Оценка":3}]

def get_mark(person):
    return person['Оценка']

person_marks = itertools.groupby(people, get_mark)

for key, group in person_marks:
    print(key)
    for person in group:
        print(person)
    print()