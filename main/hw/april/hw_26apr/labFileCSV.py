## todo: добавить еще своих примеры для работы с CSV-файлами
import csv

names = ["Anna", "Victor"]

with open("data.csv", "w") as file:
    writer = csv.writer(file)
    writer.writerow(
        ##names

        ("user_name", "user_password")
    )

data = [
    ["user1", "password1"],
    ["user2", "password2"],
    ["user3", "password3"],
]


for user in data:
    with open("data.csv", "a") as file:
        writer = csv.writer(file)
        writer.writerow(user)