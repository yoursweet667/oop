## todo: Создать мини-игру конвертор двоичного числа в десятичное число или шестнадцатеричное число
import time
from datetime import datetime



def choseDifficult():
    difficult = str(input('Выберете уровень сложности: детский, легкий, средний, сложный: '))
    return difficult

def choseMode():
    mode = str(input('Выберете режим игры: ответ "десятичным" числом или "шестнадцатеричным" числом: '))
    if mode == "десятичным" :
        decimal(choseDifficult())
    elif mode =="шестнадцатеричным" :
        hexadecimal(choseDifficult())



correct = 0
incorrect = 0

def counter(answer, i):

    if answer == i:
        global correct;
        correct += 1;
    else:
        global incorrect;
        incorrect += 1;



def hexadecimal(difficult):

    match difficult:
        case "детский":
            start_time = time.monotonic()
            for i in range(0, 7):
                print(bin(i))
                answer = input("Введите ответ: ")
                counter(answer, hex(i))
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "легкий":
            start_time = datetime.now()
            for i in range(0, 15):
                print(bin(i))
                answer = input("Введите ответ: ")
                counter(answer, hex(i))
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "средний":
            start_time = datetime.now()
            for i in range(0, 63):
                print(bin(i))
                answer = input("Введите ответ: ")
                counter(answer, hex(i))
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "сложный":
            start_time = datetime.now()
            for i in range(0, 255):
                print(bin(i))
                answer = hex(input("Введите ответ: "))
                counter(answer, hex(i))
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case default:
            print("Такой сложности нет!")

def decimal(difficult):

    match difficult:
        case "детский":
            start_time = time.monotonic()
            for i in range(0, 7):
                print(bin(i))
                answer = int(input("Введите ответ: "))
                counter(answer, i)
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "легкий":
            start_time = datetime.now()
            for i in range(0, 15):
                print(bin(i))
                answer = int(input("Введите ответ: "))
                counter(answer, i)
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "средний":
            start_time = datetime.now()
            for i in range(0, 63):
                print(bin(i))
                answer = int(input("Введите ответ: "))
                counter(answer, i)
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case "сложный":
            start_time = datetime.now()
            for i in range(0, 255):
                print(bin(i))
                answer = int(input("Введите ответ: "))
                counter(answer, i)
            print(f"Время игры: {time.monotonic() - start_time} секунд.")
            print(f"Правильных ответов: {correct}")
            print(f"Неправильных ответов: {incorrect}")

        case default:
            print("Такой сложности нет!")








if __name__ == "__main__":
    choseMode()

