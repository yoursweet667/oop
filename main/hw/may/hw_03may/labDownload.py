import requests

file_url = 'https://www.journaldev.com/wp-content/uploads/2019/08/Python-Tutorial.png'

file_object = requests.get(file_url)

with open('Python-Tutorial.png', 'wb') as local_file:
    local_file.write(file_object.content)