from dataclasses import dataclass


@dataclass
class FileData:

    name: str
    extension: str
    byte_size: int
    location: str
    creationDateTime: str
    modificationDatetime: str

    def total_cost(self) -> float:
        return self.unit_price * self.quantity_on_hand